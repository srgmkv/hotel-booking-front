import { Component } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
offers;
constructor(private http: HttpClient) {
}

doSearch(searchData) {
  console.log('searchData: ', searchData);

 this.offers = [];
  const url1 = 'http://localhost:3000/posts';
  this.http.get(url1)
      .pipe(delay(1000))
      .subscribe((response) => {
        console.log('response: ', response);
        this.offers = response
      });
}
}
