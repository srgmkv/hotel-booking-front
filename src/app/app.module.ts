import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { SearchFormComponent } from './search-form/search-form.component';
import { HotelOfferCardComponent } from './hotel-offer-card/hotel-offer-card.component';
import { ResultsComponent } from './results/results.component'

@NgModule({
  declarations: [
    AppComponent,
    SearchFormComponent,
    HotelOfferCardComponent,
    ResultsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
